#!/usr/bin/env -S uv run

import io
import logging
import os
import subprocess

import pika
from fdsnnetextender import FdsnNetExtender

SCRIPT_VERSION = "2024.304"

AMQP_SERVER = os.environ.get("AMQP_SERVER")
AMQP_PORT = os.environ.get("AMQP_PORT", 5672)
AMQP_VHOST = os.environ.get("RUNMODE")
AMQP_USER = os.environ.get("AMQP_USER")
AMQP_PASSWORD = os.environ.get("AMQP_PASSWORD")
AMQP_HEARTBEAT = os.environ.get("AMQP_HEARTBEAT", 600)
AMQP_TIMEOUT = os.environ.get("AMQP_TIMEOUT", 300)
SEEDTREE_EXCHANGE = os.environ.get("SEEDTREE_EXCHANGE", "post-integration")
SEEDTREE_QUEUE = os.environ.get("SEEDTREE_QUEUE", "seedtree")
SEEDTREE_ROUTING_KEY = os.environ.get("SEEDTREE_ROUTING_KEY", "miniseed")

# Logger configuration
logger = logging.getLogger(__name__)
logger.setLevel(level=logging.INFO)


def launch_seedtree_updates(channel, method, _properties, body):
    """
    Get filenames from incoming message, sort filenames by Seedtree database schema.
    Lauch each subsequent Seedtree scans.
    Send subsequent messages to applications depending on Seedtree scans.
    """
    if body is None:
        return
    body = body.decode("utf-8")
    logger.info("Received %s:%s", method.routing_key, body)


    for line in body.splitlines():
        filename = line.split("/")[-1]
        network, station, _loc, _channel, _quality, year, _day = filename.split(".")
        fullnetwork = FdsnNetExtender().extend(network, f"{year}-01-01")
        # get seedtree schema name
        schema = f"_{fullnetwork.lower()}"
        # build a stringIO containing all filenames
        filetree_stdin = io.StringIO()
        filetree_stdin.write(line + "\n")
        filetree_stdin.seek(0)
        # lauch seedtree instance
        logger.info("launching filetree --insert --forceupdate %s)", schema)
        with subprocess.Popen(
            ["filetree", "--insert", "--forceupdate", schema], stdin=subprocess.PIPE
        ) as p:
            p.communicate(filetree_stdin.read().encode())
        logger.info("lauching Seedtree (--sync %s)", schema)
        with subprocess.Popen(["seedtree", "--sync", schema]) as p:
            p.wait()
        logger.info("lauching Seedtree (--computeproducts %s)", schema)
        with subprocess.Popen(["seedtree", "--computeproducts", schema]) as p:
            p.wait()

    # notify rabbitmq server that message has been handled
    channel.basic_ack(delivery_tag=method.delivery_tag)


def main():
    logger.info("Starting")
    logger.info("Trying to connect to %s/%s", AMQP_SERVER, SEEDTREE_QUEUE)
    credentials = pika.PlainCredentials(AMQP_USER, AMQP_PASSWORD)
    parameters = pika.ConnectionParameters(
        AMQP_SERVER,
        AMQP_PORT,
        AMQP_VHOST,
        credentials,
        heartbeat=AMQP_HEARTBEAT,
        blocked_connection_timeout=AMQP_TIMEOUT,
    )
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.basic_qos(prefetch_count=1)
    channel.exchange_declare(
        exchange=SEEDTREE_EXCHANGE, exchange_type="direct", durable=True
    )
    channel.queue_declare(queue=SEEDTREE_QUEUE, durable=True)
    channel.queue_bind(
        queue=SEEDTREE_QUEUE,
        exchange=SEEDTREE_EXCHANGE,
        routing_key=SEEDTREE_ROUTING_KEY,
    )

    channel.basic_consume(
        queue=SEEDTREE_QUEUE,
        on_message_callback=launch_seedtree_updates,
        auto_ack=False,
    )
    logger.info("Start consuming the '%s' queue", SEEDTREE_QUEUE)
    channel.start_consuming()
    logger.info("amqp worker quitting")


if __name__ == "__main__":
    main()
