# À propos

`amqp_worker.py` est un script faisant partie de la chaîne d'intégration RESIF.

Il consomme des message de l'exchange rabbimq "post-integration" qui ont comme routing key "miniseed". À chaque message, il va mettre à jour Seedtree pour le fichier contenu dans le message

Le tout est décrit dans le wiki RESIF : https://wiki.osug.fr/!isterre-geodata/resif/systemes/services/integration_n%C5%93ud_b

Script original par Pierre Volcke.

# Installation

Ce script nécessite des dépendances. Pour les installer, executer la commande suivante : 

``` bash
uv sync --frozen
```

Les variables d'environnement suivantes doivent être initialisées : 
```
RUNMODE
AMQP_SERVER
AMQP_USER
AMQP_PASSWORD
```

Créer des wrappers pour filtree et seedtree, exécutables, dans ~/.local/bin :

`~/bin/filetree`
``` bash
#!/bin/bash

# Wrapper pour lancer filetree

SEEDTREE_PATH=/home/sysop/seedtree5/src
PYTHON=~/.venv/seedtree/bin/python

$PYTHON $SEEDTREE_PATH/filetree.py $@
```

et `~/bin/seedtree`

``` bash
#!/bin/bash

# Wrapper pour lancer filetree

SEEDTREE_PATH=/home/sysop/seedtree5/src
PYTHON=~/.venv/seedtree/bin/python

echo "$PYTHON $SEEDTREE_PATH/seedtree5.py $@"
$PYTHON $SEEDTREE_PATH/seedtree5.py $@
```



